const mongoose = require("mongoose");
const Joi = require("@hapi/joi");
mongoose.set("useCreateIndex", true);

const filmSchema = mongoose.Schema({
  id: { type: String, required: true, unique: true },
  name: { type: String, required: true },
  content: { type: String, required: true },
  type: { type: [String], required: true, default: undefined },
  origin: { type: String, required: true },
  duration: { type: String, required: true },
  media: { type: String, required: true },
  releasedDate: { type: String, required: true }
});

function validationCreateFilm(value) {
  const Schema = Joi.object().keys({
    id: Joi.string().required(),
    name: Joi.string().required(),
    content: Joi.string().required(),
    type: Joi.array().items(Joi.string()),
    origin: Joi.string().required(),
    duration: Joi.string().required(),
    media: Joi.string().required(),
    releasedDate: Joi.string().required()
  });
  return Joi.validate(value, Schema);
}

module.exports = {
  Film: mongoose.model("Film", filmSchema),
  validationCreateFilm: validationCreateFilm
};
