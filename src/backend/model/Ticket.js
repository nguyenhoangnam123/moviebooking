const mongoose = require("mongoose");
const Joi = require("@hapi/joi");
Joi.objectId = require("joi-objectid")(Joi);
mongoose.set("useCreateIndex", true);

const ObjectId = mongoose.Schema.Types.ObjectId;

const ticketSchema = mongoose.Schema({
  id: { type: String, required: true },
  schedule: {
    id: { type: String, required: true },
    film: {
      id: { type: String, required: true },
      name: { type: String, required: true },
      content: { type: String, required: true },
      duration: { type: String, required: true }
    },
    room: { type: String, required: true },
    date: { type: String, required: true },
    time: { type: String, required: true }
  },
  seat: {
    id: { type: String, required: true },
    seat_number: { type: String, required: true },
    seat_type: { type: String, required: true }
  },
  price: { type: Number, required: true },
  user: {
    _id: { type: ObjectId, required: true },
    email: { type: String, required: true }
  }
});

ticketSchema.index({ schedule: 1, seat: 1 }, { unique: true });

function validateCreateTicket(value) {
  const schema = Joi.object().keys({
    id: Joi.string().required(),
    schedule: {
      _id: Joi.string(),
      id: Joi.string().required(),
      film: {
        id: Joi.string().required(),
        name: Joi.string().required(),
        content: Joi.string(),
        duration: Joi.string().required()
      },
      room: Joi.string().required(),
      date: Joi.string().required(),
      emptySeat: Joi.number(),
      time: Joi.string().required(),
      __v: Joi.number()
    },
    seat: {
      id: Joi.string().required(),
      seat_number: Joi.string().required(),
      seat_type: Joi.string().required()
    },
    price: {
      film: Joi.string(),
      seat_type: Joi.string(),
      value: Joi.number()
    },
    user: {
      _id: Joi.objectId().required(),
      email: Joi.string()
        .email()
        .required()
    }
  });
  return Joi.validate(value, schema);
}

module.exports = {
  Ticket: mongoose.model("Ticket", ticketSchema),
  validateCreateTicket: validateCreateTicket
};
