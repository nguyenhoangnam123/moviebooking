const mongoose = require("mongoose");
const Joi = require("@hapi/joi");
const objectId = require("joi-objectid");
const bcrypt = require("bcrypt");
const saltRounds = 10;
mongoose.set("useCreateIndex", true);

const userSchema = mongoose.Schema({
  firstname: { type: String, required: true },
  lastname: { type: String, required: true },
  gender: { type: String, required: true },
  age: { type: Number, required: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  roll: { type: String, required: true, default: "normal member" }
});

userSchema.pre("save", function(next) {
  // Check if document is new or a new password has been set
  if (this.isNew || this.isModified("password")) {
    // Saving reference to this because of changing scopes
    const document = this;
    bcrypt.hash(document.password, saltRounds, function(err, hashedPassword) {
      if (err) {
        next(err);
      } else {
        document.password = hashedPassword;
        next();
      }
    });
  } else {
    next();
  }
});

userSchema.methods.isCorrectPassword = function(password, callback) {
  bcrypt.compare(password, this.password, function(err, same) {
    if (err) {
      callback(err);
    } else {
      callback(err, same);
    }
  });
};

function validateSignUp(value) {
  const schema = Joi.object().keys({
    firstname: Joi.string().required(),
    lastname: Joi.string().required(),
    gender: Joi.string().required(),
    age: Joi.number()
      .integer()
      .min(5)
      .max(120)
      .required(),
    email: Joi.string()
      .email()
      .required(),
    password: Joi.string()
      .min(10)
      .max(1024)
      .required()
  });
  return Joi.validate(value, schema);
}

function validateLogin(value) {
  const schema = Joi.object().keys({
    email: Joi.string()
      .email()
      .required(),
    password: Joi.string().required()
  });
  return Joi.validate(value, schema);
}

module.exports = {
  User: mongoose.model("User", userSchema),
  validateSignUp: validateSignUp,
  validateLogin: validateLogin
};
