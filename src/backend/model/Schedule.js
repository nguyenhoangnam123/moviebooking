const mongoose = require("mongoose");
const Joi = require("@hapi/joi");
mongoose.set("useCreateIndex", true);

const scheduleSchema = mongoose.Schema({
  id: { type: String, required: true },
  film: {
    id: { type: String, required: true },
    name: { type: String, required: true },
    content: { type: String, required: true },
    duration: { type: String, required: true }
  },
  room: { type: String, required: true },
  date: { type: String, required: true },
  emptySeat: { type: Number, required: true },
  time: { type: String, required: true }
});

function validationCreateSchedule(value) {
  const schema = Joi.object().keys({
    id: Joi.string().required(),
    film: {
      id: Joi.string().required(),
      name: Joi.string().required(),
      content: Joi.string().required(),
      duration: Joi.string().required()
    },
    room: Joi.string().required(),
    date: Joi.string().required(),
    emptySeat: Joi.number().required(),
    time: Joi.string().required()
  });
  return Joi.validate(value, schema);
}

scheduleSchema.index({ film: 1, room: 1, date: 1, time: 1 }, { unique: true });

module.exports = {
  Schedule: mongoose.model("Schedule", scheduleSchema),
  validationCreateSchedule: validationCreateSchedule
};
