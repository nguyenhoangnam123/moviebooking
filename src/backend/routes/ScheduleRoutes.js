const router = require("express").Router();
const withAuth = require("../middleware/userMiddleware");
const { Schedule, validationCreateSchedule } = require("../model/Schedule");

router.post("/create", withAuth, async (req, res) => {
  const schedule = req.body;
  const { error } = validationCreateSchedule(schedule);
  if (error) {
    res.status(400).json({ message: error.message });
  } else {
    try {
      await Schedule.create(schedule);
      res.status(200).send("ok");
    } catch (ex) {
      console.log(ex.message);
      res.status(500).json({ message: "something wrong" });
    }
  }
});

router.get("/findScheduleOfFilm/:id", async (req, res) => {
  const id = req.params.id;
  try {
    const result = await Schedule.find({ "film.id": id });
    res.status(200).json(result);
  } catch (ex) {
    console.log(ex.message);
    res.status(500).json({ message: "something wrong" });
  }
});

module.exports = router;
