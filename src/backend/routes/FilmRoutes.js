const router = require("express").Router();
const { Film, validationCreateFilm } = require("../model/Film");
const withAuth = require("../middleware/userMiddleware");

router.post("/create", withAuth, async (req, res) => {
  const film = req.body;
  const { error } = validationCreateFilm(film);
  if (error) {
    res.status(400).json({ message: "bad request" });
  } else {
    try {
      const response = await Film.create(film);
      res.status(200).send(response);
    } catch (ex) {
      console.log(ex.message);
      res.status(500).json({ message: "something wrong" });
    }
  }
});

router.get("/findAll", async (req, res) => {
  try {
    const response = await Film.find({});
    res.status(200).send(response);
  } catch (ex) {
    console.log(ex.message);
    res.status(500).json({ message: "something wrong" });
  }
});

module.exports = router;
