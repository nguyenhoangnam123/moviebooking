const router = require("express").Router();
const withAuth = require("../middleware/userMiddleware");
const { Ticket, validateCreateTicket } = require("../model/Ticket");
const { User } = require("../model/User");

router.post("/create", withAuth, async (req, res) => {
  const tickets = req.body.tickets;
  try {
    tickets.map(async ticket => {
      const { error } = validateCreateTicket(ticket);
      if (error) {
        console.log("error raised: ", error.message);
        return res.status(400).send(error.message);
      } else {
        console.log("everything is ok");
        const { id, schedule, seat, datetime, price, user } = ticket;
        const { _id, email } = user;
        const authUser = await User.findOne({ _id }, { email });
        const oldTicket = await Ticket.findOne({ seat });
        if (!authUser) {
          return res.status(403).json({ message: "you are unauthorized" });
        } else if (oldTicket) {
          return res
            .status(400)
            .json({ message: "you have already booked this seat" });
        } else {
          const response = await Ticket.create({
            id,
            schedule,
            seat,
            datetime,
            price: price.value,
            user
          });
          return res.status(200).json({ message: "successfully" });
        }
      }
    });
  } catch (ex) {
    console.log(ex.message);
    return res.status(500).send({ message: "something wrong" });
  }
});

router.get("/findTicketOfSchedule/:id", async (req, res) => {
  const id = req.params.id;
  try {
    const result = await Ticket.find({ "schedule.id": id });
    res.status(200).json({ value: result });
  } catch (ex) {
    console.log(ex.message);
    res.status(500).json({ message: "something wrong" });
  }
});

router.get("/findAll", async (req, res) => {
  try {
    const data = await Ticket.find({});
    console.log(data);
    res.status(200).send(data);
  } catch (ex) {
    console.log(ex.message);
    res.status(500).json({ message: "something wrong" });
  }
});

router.delete("/delete", withAuth, async (req, res) => {
  const deletedIds = req.body;
  try {
    deletedIds.map(async id => {
      await Ticket.findOneAndDelete({ _id: id });
    });
  } catch (ex) {
    console.log(ex.message);
    res.status(500).json({ message: "something wrong" });
  }
});

module.exports = router;
