const router = require("express").Router();
const { User, validateSignUp, validateLogin } = require("../model/User");
const jwt = require("jsonwebtoken");
const withAuth = require("../middleware/userMiddleware");
require("dotenv").config();

const { SECRET } = process.env;

router.get("/", async (req, res) => {
  try {
    const users = await User.find().sort("firstname");
    res.send(users);
  } catch (ex) {
    console.log(ex);
  }
});

router.post("/create", async (req, res) => {
  const { error } = validateSignUp(req.body);
  if (error) {
    return res.status(400).send(error.message);
  } else {
    const { firstname, lastname, gender, age, email, password } = req.body;
    const checkEmailExist = await User.find({ email });
    if (!checkEmailExist) {
      res.status(400).json({
        error: "email existed"
      });
    } else {
      try {
        const response = await User.create({
          firstname,
          lastname,
          gender,
          age,
          email,
          password
        });
        res.status(200).send(response);
      } catch (ex) {
        const err = new Error(ex.message);
        res.status(500).send(ex.message);
      }
    }
  }
});

router.put("/update", async (req, res) => {
  const { error } = validateSignUp(req.body);
  if (error) {
    console.log("error===>", error);
    return res.status(400).send(error.message);
  } else {
    res.status(200).send("form value is acceptable");
    console.log("ok");
  }
});

router.post("/login", async (req, res) => {
  const { error } = validateLogin(req.body);
  if (error) {
    console.log("error===>", error);
    return res.status(400).send(error.message);
  } else {
    try {
      const { email, password } = req.body;
      const user = await User.findOne({ email });
      if (!user) {
        res.status(401).json({ error: "email is not exist" });
      } else {
        user.isCorrectPassword(password, function(err, same) {
          if (err) {
            res.status(500).json({
              error: "Internal error please try again"
            });
          } else if (!same) {
            res.status(401).json({
              error: "Incorrect password"
            });
          } else {
            // Issue token
            const payload = { email };
            const token = jwt.sign(payload, SECRET, {
              expiresIn: "1h"
            });
            res
              .cookie("token", token, { httpOnly: true })
              .status(200)
              .json({ _id: user._id, email: user.email });
          }
        });
      }
    } catch (ex) {
      const err = new Error(ex.message);
      res.status(500).send(ex.message);
    }
  }
});

router.get("/checkToken", withAuth, (req, res) => {
  res.sendStatus(200);
});

module.exports = router;
