const express = require("express");
const mongoose = require("mongoose");
const config = require("./config");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const user = require("./routes/UserRoutes");
const ticket = require("./routes/TicketRoutes");
const film = require("./routes/FilmRoutes");
const schedule = require("./routes/ScheduleRoutes");

const app = express();
app.use(express.static("./src/assets"));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use("/user", user);
app.use("/ticket", ticket);
app.use("/film", film);
app.use("/schedule", schedule);
mongoose
  .connect(config.url, { useNewUrlParser: true })
  .then(() => console.log("successfully"))
  .catch(err => console.log(err));

// require("./routes/UserRoutes");

app.listen(3000);
