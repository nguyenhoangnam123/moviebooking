import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import red from "@material-ui/core/colors/red";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      flex: "0 0 30%",
      margin: theme.spacing(2)
    },
    media: {
      height: 0,
      paddingTop: "56.25%" // 16:9
    },
    expand: {
      transform: "rotate(0deg)",
      marginLeft: "auto",
      transition: theme.transitions.create("transform", {
        duration: theme.transitions.duration.shortest
      })
    },
    expandOpen: {
      transform: "rotate(180deg)"
    },
    avatar: {
      backgroundColor: red[500]
    },
    cardBodyText: {
      textAlign: "left",
      marginBottom: theme.spacing(1)
    }
  })
);

export default useStyles;
