import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      position: "relative"
    },
    title: {
      marginLeft: theme.spacing(2),
      flex: 1
    },
    dateListItem: {
      flex: "start",
      display: "flex",
      width: 100,
      border: "1px solid #343434",
      margin: 10,
      borderRadius: 3
    },
    dateListItemText: {
      flex: "2 0 0",
      textAlign: "center",
      fontSize: "1.5rem",
      fontWeight: 700
    },
    locationListItem: {
      flex: "start",
      display: "flex",
      width: 200,
      background: "#343434",
      color: "#f0f0f0",
      margin: 10,
      borderRadius: 3
    },
    locationListItemText: {
      flex: "1 0 0",
      textAlign: "center"
    }
  })
);
export default useStyles;
