import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(3)
  },
  paper: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(3),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  seatTableList: {
    display: "flex"
  },
  seatTableListText: {
    flex: "0 0 45%",
    textAlign: "left"
  },
  leftIcon: {
    marginRight: theme.spacing(1)
  },
  iconSmall: {
    fontSize: 20
  },
  button: {
    padding: theme.spacing(1),
    marginRight: theme.spacing(1)
  }
}));

export default useStyles;
