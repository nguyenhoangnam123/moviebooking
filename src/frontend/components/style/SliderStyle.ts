import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import { orange } from "@material-ui/core/colors";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    slider: {
      display: "flex",
      background: orange[500],
      justifyContent: "center",
      padding: `${theme.spacing(2)}px 0`,
      marginBottom: theme.spacing(3)
    },
    sliderItem: {
      flex: "0 0 70%",
      height: 400,
      backgroundSize: "cover",
      backgroundRepeat: "none",
      backgroundPosition: "bottom center",
      position: "relative"
    },
    leftArrow: {
      position: "absolute",
      left: "0",
      lineHeight: "400px"
    },
    rightArrow: {
      position: "absolute",
      lineHeight: "400px",
      right: "0"
    },
    iconChevron: {
      fontSize: 64,
      background: "rgba(0,0,0,.5)",
      color: "#f0f0f0"
    },
    popCornContainer: {
      flex: "0 0 15%",
      position: "relative"
    },
    popCorn: {
      position: "absolute",
      left: 0,
      bottom: "-16px",
      width: "100%"
    },
    hotMovieContainer: {
      height: "400px",
      overflow: "auto",
      flex: "0 0 15%",
      background: "#343434"
    },
    listItemText: {
      color: "#fff"
    }
  })
);

export default useStyles;
