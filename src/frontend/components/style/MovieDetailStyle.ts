import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import { flexbox } from "@material-ui/system";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      display: "flex",
      height: "100vh",
      alignItems: "center",
      justifyContent: "center"
    },
    container: {
      paddingLeft: theme.spacing(5),
      paddingRight: theme.spacing(5)
    },
    paper: {
      padding: theme.spacing(3),
      textAlign: "center",
      color: theme.palette.text.secondary,
      width: "80vw",
      height: "60vh"
    },
    imageFrame: {
      width: "100%",
      height: "100%",
      display: "block",
      backgroundSize: "cover !important",
      backgroundRepeat: "no-repeat !important"
    }
  })
);

export default useStyles;
