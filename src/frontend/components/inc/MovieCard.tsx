import * as React from "react";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import MovieIcon from "@material-ui/icons/Movie";
import EventIcon from "@material-ui/icons/Event";
import { Link, Route } from "react-router-dom";
import { Film, TicketContainerProps } from "Interface/index";
import { MovieCardStyle } from "../style/index";

interface MovieCardProps {
  film: Film;
  ticketContainer: TicketContainerProps;
}

const MovieCard = ({ film, ticketContainer }: MovieCardProps) => {
  const { updateChange, openModal } = ticketContainer;
  const classes = MovieCardStyle();
  const { name, content, media, releasedDate, type, id, duration } = film;

  const handleClick = () => {
    openModal();
    updateChange("chosenFilm", film);
    const loadData = async () => {
      try {
        const response = await fetch(`/schedule/findScheduleOfFilm/${id}`);
        const schedules = await response.json();
        console.log(schedules);
        updateChange("schedules", schedules);
      } catch (ex) {
        console.log(ex.message);
      }
    };
    loadData();
  };

  return (
    <Card className={classes.card}>
      <Link
        to={"/movie/" + id}
        style={{ textDecoration: "none" }}
        onClick={() => updateChange("chosenFilm", film)}
      >
        <CardHeader
          avatar={
            <Avatar aria-label="Recipe" className={classes.avatar}>
              FX
            </Avatar>
          }
          title={
            <Typography
              variant="h6"
              component="h6"
              color="textSecondary"
              style={{ fontWeight: "bold" }}
            >
              {name}
            </Typography>
          }
          subheader={releasedDate}
        />
        <CardMedia
          className={classes.media}
          image={media}
          title="Paella dish"
        />
        <CardContent>
          <Typography
            variant="body2"
            color="textSecondary"
            component="p"
            className={classes.cardBodyText}
          >
            {content}
          </Typography>
          <Typography
            variant="body2"
            component="h6"
            className={classes.cardBodyText}
            color="textSecondary"
          >
            Duration: {duration}
          </Typography>
          <Typography
            variant="body2"
            color="textSecondary"
            component="p"
            className={classes.cardBodyText}
          >
            Type:{" "}
            {type.map((item, index) => {
              return <span key={index}>{item + " "}</span>;
            })}
          </Typography>
        </CardContent>
      </Link>
      <CardActions disableSpacing>
        <Link to={"/movie/" + id}>
          <IconButton aria-label="See details">
            <MovieIcon />
          </IconButton>
        </Link>

        <IconButton aria-label="book ticket" onClick={handleClick}>
          <EventIcon />
        </IconButton>
      </CardActions>
    </Card>
  );
};

export default MovieCard;
