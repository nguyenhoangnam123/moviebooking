import * as React from "react";
import { Subscribe } from "unstated-x";
import TicketContainer from "Components/containers/TicketContainer";
import SeatDetails from "./SeatDetails";
import ScheduleDetails from "./ScheduleDetails";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import SnackBar from "../inc/SnackBar";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(3)
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  }
}));

const BookingDetails = () => {
  const { root, paper } = useStyles();
  const [data, setData] = React.useState({
    message: "",
    status: false,
    variant: "success"
  });

  const handleClose = () => {
    setData({ ...data, status: false });
  };

  const setSnackBar = (message: string, variant: string) => {
    setData({ variant, message, status: true });
  };

  return (
    <Subscribe to={[TicketContainer]}>
      {(ticketContainer: TicketContainer) => (
        <div className={root}>
          <Grid container spacing={3}>
            <Grid item xs={9}>
              <Paper className={paper}>
                <SeatDetails
                  ticketContainer={ticketContainer}
                  setSnackBar={setSnackBar}
                />
              </Paper>
            </Grid>
            <Grid item xs={3}>
              <ScheduleDetails
                ticketContainer={ticketContainer}
                setSnackBar={setSnackBar}
              />
            </Grid>
          </Grid>
          <SnackBar data={data} handleClose={handleClose} />
        </div>
      )}
    </Subscribe>
  );
};

export default BookingDetails;
