import * as React from "react";
import { TicketContainerProps } from "Interface/index";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";

interface SeatDetailsProps {
  ticketContainer: TicketContainerProps;
  setSnackBar: (message: string, variant: string) => void;
}

const SeatDetails = ({ ticketContainer, setSnackBar }: SeatDetailsProps) => {
  const { state, addChosenSeat, updateChange } = ticketContainer;
  const { seat, fullFillSeat, chosenSchedule } = state;

  React.useEffect(() => {
    const fullFillSeat = async () => {
      try {
        const response = await fetch(
          `/ticket/findTicketOfSchedule/${chosenSchedule.id}`
        );
        const data = await response.json();
        if (response.status == 200) {
          const tickets = data.value;
          const seats = tickets.map((ticket: any) => ticket.seat);
          updateChange("fullFillSeat", seats);
        }
      } catch (ex) {
        console.log(ex.message);
      }
    };
    fullFillSeat();
  }, []);

  const handleClick = (e: React.MouseEvent) => {
    const node = e.currentTarget!;
    const status = node.getAttribute("data-id");
    const seat_number = node.getAttribute("data-seat-number")!;
    if (status == "full") {
      console.log("seat_number", seat_number);
      setSnackBar("That seat is reserved", "warning");
    } else {
      console.log("seat_number", seat_number);
      setSnackBar("Picking seat successfully", "success");
      addChosenSeat(seat_number);
    }
  };

  return (
    <List style={{ display: "flex", flexWrap: "wrap" }}>
      {seat.map((seat, index) => {
        const { seat_number, seat_type } = seat;
        const fullFillSeatName = fullFillSeat.map(item => item.seat_number);
        return (
          <ListItem button key={index} style={{ flex: "0 0 10%" }}>
            {fullFillSeatName.indexOf(seat.seat_number) != -1 ? (
              <ListItemText
                primary={seat_number}
                secondary={seat_type}
                data-id="full"
                data-seat-number={seat_number}
                style={{
                  textAlign: "center",
                  border: "1px solid rgba(225, 0, 80, 1)",
                  borderRadius: "5px",
                  color: "#fff",
                  backgroundColor: "rgba(225, 0, 80, .5)"
                }}
                onClick={handleClick}
              />
            ) : (
              <ListItemText
                primary={seat_number}
                secondary={seat_type}
                data-id="empty"
                data-seat-number={seat_number}
                style={{
                  textAlign: "center",
                  border: "1px solid #343434",
                  borderRadius: "5px"
                }}
                onClick={handleClick}
              />
            )}
          </ListItem>
        );
      })}
    </List>
  );
};

export default SeatDetails;
