import * as React from "react";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import { SliderStyle } from "../style/index";
import { Film } from "Interface/index";
import { Link, Route } from "react-router-dom";

interface SliderItemProps {
  title: string;
  src: string;
  goPrev: () => void;
  goNext: () => void;
}
interface LeftArrow {
  goPrev: () => void;
}
interface RightArrow {
  goNext: () => void;
}

interface HotMovieProps {
  films: Film[];
}

interface SliderProps {
  films: Film[];
}

const LeftArrow = ({ goPrev }: LeftArrow) => {
  const { leftArrow, iconChevron } = SliderStyle();
  return (
    <div className={leftArrow} onClick={() => goPrev()}>
      <ChevronLeftIcon className={iconChevron} />
    </div>
  );
};

const RightArrow = ({ goNext }: RightArrow) => {
  const { rightArrow, iconChevron } = SliderStyle();
  return (
    <div className={rightArrow} onClick={() => goNext()}>
      <ChevronRightIcon className={iconChevron} />
    </div>
  );
};

const SliderItem = ({ src, goPrev, goNext }: SliderItemProps) => {
  const { sliderItem } = SliderStyle();
  return (
    <div className={sliderItem} style={{ backgroundImage: `url(${src})` }}>
      <LeftArrow goPrev={goPrev} />
      <RightArrow goNext={goNext} />
    </div>
  );
};

const HotMovie = ({ films }: HotMovieProps) => {
  const { hotMovieContainer, listItemText } = SliderStyle();
  return (
    <div className={hotMovieContainer}>
      {films &&
        films.map((item, index) => {
          const { name, releasedDate, id } = item;
          return (
            <Link to={"/movie/" + id} key={index}>
              <ListItem alignItems="flex-start">
                <ListItemText
                  className={listItemText}
                  primary={name}
                  secondary={
                    <Typography
                      component="p"
                      variant="body2"
                      color="textPrimary"
                      style={{ color: "#f8f8f8" }}
                    >
                      Released Date: {releasedDate}
                    </Typography>
                  }
                />
              </ListItem>
              <Divider
                variant="inset"
                component="hr"
                style={{ background: "gray" }}
              />
            </Link>
          );
        })}
    </div>
  );
};

const PopCorn = () => {
  const { popCornContainer, popCorn } = SliderStyle();
  return (
    <div className={popCornContainer}>
      <img
        src="http://pngimg.com/uploads/popcorn/popcorn_PNG41.png"
        className={popCorn}
      />
    </div>
  );
};

const Slider = ({ films }: SliderProps) => {
  const { slider } = SliderStyle();
  const [step, setStep] = React.useState(0);
  const SliderContent = [
    {
      title: "lion king",
      src:
        "https://static5.businessinsider.com/image/5c739bc3dde86718564ddaaa-1688/the-lion-king-poster.jpg"
    },
    {
      title: "aladin",
      src:
        "https://lumiere-a.akamaihd.net/v1/images/b_aladdin_header_17903_4ec6b753.jpeg?region=0,0,2048,640"
    },
    {
      title: "engame",
      src:
        "https://lumiere-a.akamaihd.net/v1/images/b_avengersendgame_horizontal_nowplaying_17793_5531b541.jpeg?region=0,0,2048,599"
    }
  ];

  const goNext = () => {
    if (step == 2) {
      setStep(0);
    } else {
      setStep(step + 1);
    }
  };

  const goPrev = () => {
    if (step == 0) {
      setStep(2);
    } else {
      setStep(step - 1);
    }
  };

  const renderItem = (
    value: number,
    goNext: () => void,
    goPrev: () => void
  ) => {
    const { title, src } = SliderContent[value];
    return (
      <SliderItem title={title} src={src} goNext={goNext} goPrev={goPrev} />
    );
  };

  return (
    <div className={slider}>
      <PopCorn />
      {renderItem(step, goNext, goPrev)}
      <HotMovie films={films} />
    </div>
  );
};
export default Slider;
