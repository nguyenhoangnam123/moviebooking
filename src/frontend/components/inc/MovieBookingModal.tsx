import * as React from "react";
import Dialog from "@material-ui/core/Dialog";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import Slide from "@material-ui/core/Slide";
import { TransitionProps } from "@material-ui/core/transitions";
import { Link, Route } from "react-router-dom";
import { Schedule, TicketContainerProps } from "Interface/index";
import { MovieBookingStyle } from "../style";

interface MovieBookingModal {
  ticketContainer: TicketContainerProps;
}

const Transition = React.forwardRef<unknown, TransitionProps>(
  function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
  }
);

const MovieBookingModal = ({ ticketContainer }: MovieBookingModal) => {
  const {
    appBar,
    title,
    dateListItem,
    dateListItemText,
    locationListItem,
    locationListItemText
  } = MovieBookingStyle();
  const { state, updateChange, closeModal } = ticketContainer;
  const { location, schedules, date, chosenDate, chosenLocation, open } = state;

  const handleClick = (state: string, value: any) => {
    updateChange(state, value);
  };

  const filterSchedule = () => {
    const newSchedules: Schedule[] = [];
    schedules.map((schedule: Schedule) => {
      const { room, date } = schedule;
      if (chosenLocation.room.indexOf(room) != -1 && chosenDate.date == date) {
        newSchedules.push(schedule);
      }
    });
    return newSchedules;
  };

  React.useEffect(() => {
    const loadData = async () => {
      // update chosenFilm, chosen dates and chosen location and schedules
    };
    loadData();
  }, []);

  return (
    <>
      <Dialog
        fullScreen
        open={open}
        onClose={() => closeModal()}
        TransitionComponent={Transition}
      >
        <AppBar className={appBar} style={{ background: "#343434" }}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={() => closeModal()}
              aria-label="Close"
            >
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={title}>
              Exit
            </Typography>
          </Toolbar>
        </AppBar>
        <List style={{ display: "flex" }}>
          {date.map((item, index) => {
            const { date, day } = item;
            return (
              <ListItem
                button
                key={index}
                className={dateListItem}
                onClick={() => handleClick("chosenDate", item)}
              >
                <ListItemText
                  primary={date.slice(8, 10)}
                  secondary={day}
                  style={{
                    flex: "1 0 0"
                  }}
                />
                <ListItemText
                  primary={date.slice(5, 7)}
                  className={dateListItemText}
                />
              </ListItem>
            );
          })}
        </List>
        <Divider component="hr" />
        <List style={{ display: "flex" }}>
          {location &&
            location.map((item, index) => {
              return (
                <ListItem
                  button
                  key={index}
                  className={locationListItem}
                  onClick={() => handleClick("chosenLocation", item)}
                >
                  <ListItemText
                    primary={item.name}
                    className={locationListItemText}
                  />
                </ListItem>
              );
            })}
        </List>
        <Divider component="hr" />
        {schedules &&
          filterSchedule().map((item: any, index) => {
            const { room, time, emptySeat } = item;
            return (
              <Link
                key={index}
                to="/bookingDetail"
                style={{ textDecoration: "none" }}
                onClick={() => handleClick("chosenSchedule", item)}
              >
                <List style={{ display: "flex" }}>
                  <ListItem
                    button
                    style={{
                      width: 300,
                      flexDirection: "column",
                      justifyContent: "center"
                    }}
                  >
                    <ListItemText
                      primary={room}
                      style={{
                        textAlign: "left",
                        color: "#343434"
                      }}
                    />
                    <ListItemText
                      primary={time}
                      secondary={emptySeat + " vacancies"}
                      style={{
                        width: 200,
                        textAlign: "center",
                        fontSize: "24px",
                        border: "1px solid #343434",
                        borderRadius: "3px"
                      }}
                    />
                  </ListItem>
                </List>
                <Divider component="hr" />
              </Link>
            );
          })}
      </Dialog>
    </>
  );
};

const loadSchedules = async (action: any) => {
  try {
    const response = await fetch(
      "/api/schedule/2019-06-15/08:00:00/123456780Abc",
      {
        mode: "cors",
        method: "GET"
      }
    );
    const data = await response.json();
    const { location, schedule } = data;
    action(location, schedule);
  } catch (ex) {
    console.log(ex);
  }
};

export default MovieBookingModal;
