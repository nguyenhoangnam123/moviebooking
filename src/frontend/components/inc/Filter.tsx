import * as React from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import { Paper } from "@material-ui/core";
import { Date } from "Interface/ITicketContainer";

interface FilterProps {
  style: {
    paper: string;
    root: string;
  };
  datetime: Date[];
}

const Filter = ({ style, datetime }: FilterProps) => {
  const { paper, root } = style;
  const [checked, setChecked] = React.useState([0]);

  const handleToggle = (value: number) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  return (
    <Paper className={paper}>
      <List className={root}>
        {datetime &&
          datetime.map((value, index) => {
            const labelId = `checkbox-list-label-${value}`;

            return (
              <ListItem
                key={index}
                role={undefined}
                dense
                button
                onClick={handleToggle(index)}
              >
                <ListItemIcon>
                  <Checkbox
                    edge="start"
                    checked={checked.indexOf(index) !== -1}
                    tabIndex={-1}
                    disableRipple
                    inputProps={{ "aria-labelledby": labelId }}
                  />
                </ListItemIcon>
                <ListItemText id={labelId} primary={`${value.date}`} />
              </ListItem>
            );
          })}
      </List>
    </Paper>
  );
};

export default Filter;
