import * as React from "react";
import clsx from "clsx";
import { TicketContainerProps, Ticket } from "Interface/index";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import DeleteForeverIcon from "@material-ui/icons/DeleteForeverTwoTone";
import SaveIcon from "@material-ui/icons/Save";
import LoopIcon from "@material-ui/icons/Loop";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { ScheduleDetailsStyle } from "../style/index";

interface ScheduleDetailsProps {
  ticketContainer: TicketContainerProps;
  setSnackBar: (message: string, variant: string) => void;
}

const ScheduleDetails = ({
  ticketContainer,
  setSnackBar
}: ScheduleDetailsProps) => {
  const {
    paper,
    seatTableList,
    seatTableListText,
    button,
    iconSmall,
    leftIcon
  } = ScheduleDetailsStyle();
  const { deleteTicket, deleteAllTickets } = ticketContainer;
  const { chosenSchedule, tickets } = ticketContainer.state;
  const { film, date, time, room } = chosenSchedule;
  const { name, content, duration } = film;
  const totalCost = tickets
    .map(ticket => ticket.price.value)
    .reduce((a: number, count: number) => {
      return count + a;
    }, 0);

  const handleDeleteTicket = (ticket: Ticket) => {
    deleteTicket(ticket);
  };

  const handleDeleteAll = () => {
    deleteAllTickets();
  };

  const handleCreateTicket = async () => {
    try {
      const response = await fetch("/ticket/create", {
        method: "post",
        mode: "cors",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ tickets })
      });
      const data = await response.json();
      console.log("message: ", data.message);
    } catch (ex) {
      console.log(ex.message);
    }
  };

  return (
    <>
      <Paper className={paper}>
        <Typography align="left" variant="h6" color="initial">
          Film Details
        </Typography>
        <Typography align="left" variant="body1" color="textSecondary">
          title: {name}
        </Typography>
        <Typography align="left" variant="body1" color="initial">
          content: {content}
        </Typography>
        <Typography align="left" variant="body1" color="initial">
          duration: {duration}
        </Typography>
      </Paper>
      <Paper className={paper}>
        <Typography align="left" variant="subtitle1" color="initial">
          Date: {date}
        </Typography>
        <Typography align="left" variant="subtitle1" color="initial">
          Time: {time}
        </Typography>
      </Paper>
      <Paper className={paper}>
        <Typography align="left" variant="h6" color="initial">
          Room and Seat
        </Typography>
        <Typography align="left" variant="subtitle1" color="initial">
          Room: {room}
        </Typography>
        <List>
          <ListItem className={seatTableList}>
            <ListItemText primary="Seat" className={seatTableListText} />
            <ListItemText primary="Price (VND)" className={seatTableListText} />
            <ListItemSecondaryAction>Delete</ListItemSecondaryAction>
          </ListItem>
          <Divider />
          {tickets.map((ticket, index) => {
            const { seat, price, id } = ticket;
            return (
              <ListItem key={index} className={seatTableListText}>
                <ListItemText
                  primary={seat.seat_number}
                  secondary={seat.seat_type}
                  className={seatTableListText}
                />
                <ListItemText
                  primary={price.value}
                  className={seatTableListText}
                />
                <ListItemSecondaryAction>
                  {" "}
                  <IconButton
                    aria-label="Delete"
                    onClick={() => handleDeleteTicket(ticket)}
                  >
                    <DeleteIcon />
                  </IconButton>
                </ListItemSecondaryAction>
              </ListItem>
            );
          })}
          <ListItem className={seatTableList}>
            <ListItemText primary="Total" className={seatTableListText} />
            <ListItemText primary={totalCost} className={seatTableListText} />
            <ListItemSecondaryAction>
              {" "}
              <IconButton aria-label="Delete All" onClick={handleDeleteAll}>
                <DeleteForeverIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        </List>
      </Paper>
      <Button
        variant="contained"
        size="small"
        className={button}
        color="primary"
        onClick={handleCreateTicket}
      >
        <SaveIcon className={clsx(leftIcon, iconSmall)} />
        Book now
      </Button>
      <Button
        variant="contained"
        size="small"
        className={button}
        color="default"
      >
        <Link to="/" style={{ color: "inherit" }}>
          <LoopIcon
            className={clsx(leftIcon, iconSmall)}
            style={{ verticalAlign: "middle" }}
          />
          Back to Home
        </Link>
      </Button>
    </>
  );
};

export default ScheduleDetails;
