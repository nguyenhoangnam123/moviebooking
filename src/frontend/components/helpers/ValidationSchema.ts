import * as Yup from "yup";

export const signUpSchema = Yup.object().shape({
  email: Yup.string()
    .required("email is required")
    .email("invalid email"),
  password: Yup.string().required("password is required"),
  firstname: Yup.string().required("firstname is required"),
  lastname: Yup.string().required("lastname is required"),
  gendername: Yup.string().required("firstname is required"),
  age: Yup.number()
    .required("age is required")
    .min(5)
    .max(120)
});

export const loginSchema = Yup.object().shape({
  email: Yup.string()
    .required("email is required")
    .email("invalid email"),
  password: Yup.string().required("password is required")
});
