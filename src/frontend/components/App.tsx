import * as React from "react";
import Form from "./userForm/Form";
import Home from "./pages/Home";
import MovieDetail from "./pages/MovieDetail";
import BookingDetail from "./inc/BookingDetails";
import UserAccount from "./pages/user/UserAccount";
import WithAuth from "./auth/withAuth";
import { BrowserRouter as Router, Link, Route } from "react-router-dom";
import { Provider } from "unstated-x";

const App = () => {
  return (
    <Router>
      <Provider>
        <Route path="/" exact render={(props: any) => <Home {...props} />} />
        <Route path="/signup" render={(props: any) => <Form {...props} />} />
        <Route path="/login" render={(props: any) => <Form {...props} />} />
        <Route
          path="/bookingDetail"
          exact
          component={WithAuth(BookingDetail)}
        />
        <Route path="/movie/:id" component={MovieDetail} />
        <Route path="/user/account" component={WithAuth(UserAccount)} />
      </Provider>
    </Router>
  );
};

export default App;
