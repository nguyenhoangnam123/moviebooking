import * as React from "react";
import UserRegisterForm from "./UserRegisterForm";
import UserDetailsForm from "./UserDetailsForm";
import Confirmation from "./Confirmation";
import UserLoginForm from "./UserLoginForm";
import { Paper } from "@material-ui/core";
import Container from "@material-ui/core/Container";
import { FormStyle } from "../style/index";
import { Formik, FormikProps, FormikActions } from "formik";
import { signUpSchema } from "../helpers/ValidationSchema";
import { FormProps } from "../../interface/index";

const Form = ({ location, history }: any) => {
  const { paper } = FormStyle();

  const [formValues, setFormValues] = React.useState<FormProps>({
    age: "",
    firstname: "",
    lastname: "",
    email: "",
    gender: "",
    password: ""
  });

  const [step, setStep] = React.useState(1);

  const handleChange = (name: keyof FormProps, value: any) => {
    console.log(`${name}: `, value);
    setFormValues({ ...formValues, [name]: value });
  };

  const nextStep = () => {
    setStep(step + 1);
  };

  const prevStep = () => {
    setStep(step - 1);
  };

  const submitFormSignUp = async () => {
    console.log("state: ", formValues);
    try {
      await fetch("/user/create", {
        method: "post",
        mode: "cors",
        headers: {
          "Content-Type": "application/json"
        },
        credentials: "same-origin",
        body: JSON.stringify(formValues)
      });
    } catch (err) {
      console.log("err: ", err);
    }
  };

  const renderSignup = (step: number, props: FormikProps<FormProps>) => {
    switch (step) {
      case 1:
        return (
          <UserRegisterForm
            nextStep={nextStep}
            {...props}
            onChangeState={handleChange}
          />
        );
      case 2:
        return (
          <UserDetailsForm
            nextStep={nextStep}
            prevStep={prevStep}
            submitFormSignUp={submitFormSignUp}
            handleChange={handleChange}
            onChangeState={handleChange}
            {...props}
          />
        );
      case 3:
        return <Confirmation />;
    }
  };

  const enhancedSignUp = (step: number) => {
    return (
      <Formik
        initialValues={{
          firstname: "",
          lastname: "",
          age: "4",
          gender: "",
          email: "",
          password: ""
        }}
        validationSchema={signUpSchema}
        onSubmit={async (
          value: FormProps,
          action: FormikActions<FormProps>
        ) => {
          console.log("form: ", value);
          action.setSubmitting(false);
        }}
        render={(props: FormikProps<FormProps>) => {
          return <>{renderSignup(step, { ...props })}</>;
        }}
      />
    );
  };

  const renderLogin = () => {
    return <UserLoginForm history={history} />;
  };

  return (
    <Container maxWidth="sm">
      <Paper className={paper}>
        {location.pathname == "/signup" ? enhancedSignUp(step) : renderLogin()}
      </Paper>
    </Container>
  );
};

export default Form;
