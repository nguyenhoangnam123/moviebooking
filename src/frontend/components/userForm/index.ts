export interface FormState {
  age: string;
  firstname: string;
  lastname: string;
  email: string;
  gender: string;
  password: string;
}
