import * as React from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { Formik, FormikProps, FormikActions } from "formik";
import { loginSchema } from "../helpers/index";

interface UserLoginProps {
  history: any;
}

interface FormValue {
  email: string;
  password: string;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    formControl: {
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary
    },
    form: {
      width: "100%", // Fix IE 11 issue.
      marginTop: theme.spacing(3)
    },
    button: {
      marginTop: theme.spacing(2)
    }
  })
);

const UserLoginForm = ({ history }: UserLoginProps) => {
  const { form, button } = useStyles();

  const onSubmitHandle = async (value: FormValue) => {
    const { email, password } = value;
    try {
      const response = await fetch("/user/login", {
        method: "post",
        mode: "cors",
        headers: {
          "Content-Type": "application/json"
        },
        credentials: "same-origin",
        body: JSON.stringify({ email, password })
      });
      if (response.status === 200) {
        const { _id, email } = await response.json();
        sessionStorage.setItem("authentication", "auth");
        sessionStorage.setItem("_id", _id);
        sessionStorage.setItem("email", email);

        history.push("/");
      } else {
        const error = new Error(response.statusText);
        throw error;
      }
    } catch (ex) {
      console.log(ex);
    }
  };

  return (
    <Formik
      initialValues={{
        email: "",
        password: ""
      }}
      validationSchema={loginSchema}
      onSubmit={async (value: FormValue, action: FormikActions<FormValue>) => {
        onSubmitHandle(value);
        action.setSubmitting(false);
      }}
      render={(props: FormikProps<FormValue>) => {
        const {
          errors,
          touched,
          isValid,
          setFieldTouched,
          handleChange,
          handleSubmit
        } = props;

        const onChangeHandle = (
          name: any,
          e: React.ChangeEvent<HTMLInputElement>
        ) => {
          e.persist();
          handleChange(e);
          setFieldTouched(name, true, false);
        };

        return (
          <form className={form} onSubmit={handleSubmit}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12}>
                <TextField
                  id="email"
                  label="Your email"
                  fullWidth
                  variant="outlined"
                  autoComplete="email"
                  name="email"
                  onChange={onChangeHandle.bind(null, "email")}
                  helperText={touched.email ? errors.email : ""}
                  error={touched.email && Boolean(errors.email)}
                />
              </Grid>
              <Grid item xs={12} sm={12}>
                <TextField
                  id="password"
                  label="Your password"
                  fullWidth
                  variant="outlined"
                  autoComplete="current-password"
                  type="password"
                  name="password"
                  onChange={onChangeHandle.bind(null, "password")}
                  helperText={touched.password ? errors.password : ""}
                  error={touched.password && Boolean(errors.password)}
                />
              </Grid>
              <Grid item xs={12}>
                <Button
                  variant="contained"
                  color="primary"
                  className={button}
                  type="submit"
                  disabled={!isValid}
                >
                  Login
                </Button>
              </Grid>
            </Grid>
          </form>
        );
      }}
    />
  );
};

export default UserLoginForm;
