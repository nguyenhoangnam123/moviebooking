import * as React from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { FormUserDetailProps } from "../../interface/index";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    formControl: {
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary
    },
    form: {
      width: "100%", // Fix IE 11 issue.
      marginTop: theme.spacing(3)
    },
    button: {
      marginTop: theme.spacing(2)
    },
    buttonDanger: {
      marginTop: theme.spacing(2),
      marginLeft: theme.spacing(2)
    }
  })
);

const UserDetailsForm = (props: FormUserDetailProps) => {
  const {
    prevStep,
    errors,
    touched,
    setFieldTouched,
    handleChange,
    submitFormSignUp,
    onChangeState,
    nextStep
  } = props;
  const { form, button, buttonDanger } = useStyles();

  const goPrev = (e: React.MouseEvent) => {
    e.preventDefault();
    prevStep();
  };

  const onSubmitHandle = async (e: React.FormEvent) => {
    e.preventDefault();
    e.persist();
    await submitFormSignUp();
    nextStep();
  };

  const onChangeHandle = (
    name: any,
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    e.persist();
    handleChange(e);
    onChangeState(e.target.name, e.target.value);
    setFieldTouched(name, true, false);
  };

  return (
    <form className={form} onSubmit={onSubmitHandle}>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={12}>
          <TextField
            id="firstname"
            label="Your Firstname"
            fullWidth
            variant="outlined"
            autoComplete="name"
            name="firstname"
            onChange={onChangeHandle.bind(null, "firstname")}
            helperText={touched.firstname ? errors.firstname : ""}
            error={touched.firstname && Boolean(errors.firstname)}
          />
        </Grid>
        <Grid item xs={12} sm={12}>
          <TextField
            id="lastname"
            label="Your Lastname"
            fullWidth
            variant="outlined"
            type="text"
            name="lastname"
            onChange={onChangeHandle.bind(null, "lastname")}
            helperText={touched.lastname ? errors.lastname : ""}
            error={touched.lastname && Boolean(errors.lastname)}
          />
        </Grid>
        <Grid item xs={12} sm={12}>
          <TextField
            id="gender"
            label="Your Gender"
            fullWidth
            variant="outlined"
            type="text"
            name="gender"
            onChange={onChangeHandle.bind(null, "gender")}
            helperText={touched.gender ? errors.gender : ""}
            error={touched.gender && Boolean(errors.gender)}
          />
        </Grid>
        <Grid item xs={12} sm={12}>
          <TextField
            id="age"
            label="Your Age"
            fullWidth
            variant="outlined"
            type="text"
            name="age"
            onChange={onChangeHandle.bind(null, "age")}
            helperText={touched.age ? errors.age : ""}
            error={touched.age && Boolean(errors.age)}
          />
        </Grid>
        <Grid item xs={12}>
          <Button
            variant="contained"
            color="primary"
            className={button}
            type="submit"
          >
            Submit
          </Button>
          <Button
            variant="contained"
            color="secondary"
            onClick={goPrev}
            className={buttonDanger}
          >
            Go back
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default UserDetailsForm;
