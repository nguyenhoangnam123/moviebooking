import * as React from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { FormRegistrationProps } from "../../interface/index";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    formControl: {
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary
    },
    form: {
      width: "100%", // Fix IE 11 issue.
      marginTop: theme.spacing(3)
    },
    button: {
      marginTop: theme.spacing(2)
    }
  })
);

const UserRegisterForm = (props: FormRegistrationProps) => {
  const {
    nextStep,
    errors,
    touched,
    setFieldTouched,
    handleChange,
    onChangeState
  } = props;

  const { form, button } = useStyles();

  const goNext = (e: React.MouseEvent) => {
    e.preventDefault();
    nextStep();
  };

  const onChangeHandle = (
    name: any,
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    e.persist();
    handleChange(e);
    onChangeState(e.target.name, e.target.value);
    setFieldTouched(name, true, false);
  };

  return (
    <form className={form}>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={12}>
          <TextField
            id="email"
            label="Your email"
            fullWidth
            variant="outlined"
            autoComplete="email"
            name="email"
            onChange={onChangeHandle.bind(null, "email")}
            helperText={touched.email ? errors.email : ""}
            error={touched.email && Boolean(errors.email)}
          />
        </Grid>
        <Grid item xs={12} sm={12}>
          <TextField
            id="password"
            label="Your password"
            fullWidth
            variant="outlined"
            autoComplete="current-password"
            type="password"
            name="password"
            onChange={onChangeHandle.bind(null, "password")}
            helperText={touched.password ? errors.password : ""}
            error={touched.password && Boolean(errors.password)}
          />
        </Grid>
        <Grid item xs={12}>
          <Button
            variant="contained"
            color="primary"
            onClick={goNext}
            className={button}
          >
            Continue
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default UserRegisterForm;
