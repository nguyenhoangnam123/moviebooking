import * as React from "react";
import MovieCard from "../inc/MovieCard";
import MovieBookingModal from "../inc/MovieBookingModal";
import TicketContainer from "Components/containers/TicketContainer";
import { Subscribe } from "unstated-x";
import { Film } from "Interface/index";

interface RightSideProps {
  films: Film[];
}

const RightSide = ({ films }: RightSideProps) => {
  return (
    <div
      style={{
        display: "flex",
        flexWrap: "wrap"
      }}
    >
      <Subscribe to={[TicketContainer]}>
        {(ticketContainer: TicketContainer) => (
          <>
            {films &&
              films.map((film: Film, index: any) => {
                return (
                  <React.Fragment key={index}>
                    <MovieCard film={film} ticketContainer={ticketContainer} />
                  </React.Fragment>
                );
              })}

            <MovieBookingModal ticketContainer={ticketContainer} />
          </>
        )}
      </Subscribe>
    </div>
  );
};

export default RightSide;
