import * as React from "react";
import Category from "../inc/Category";
import Filter from "../inc/Filter";
import { makeStyles } from "@material-ui/core/styles";
import { Date } from "Interface/ITicketContainer";

interface LeftSide {
  datetime: Date[];
}

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
    marginBottom: theme.spacing(3)
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    marginBottom: theme.spacing(3)
  }
}));

const LeftSide = ({ datetime }: LeftSide) => {
  const classes = useStyles();
  return (
    <>
      <Filter style={classes} datetime={datetime} />
    </>
  );
};

export default LeftSide;
