import * as React from "react";
import Navbar from "../inc/Navbar";
import Slider from "../inc/Slider";
import LeftSide from "./LeftSide";
import RightSide from "./RightSide";
import { Paper } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import { HomeStyle } from "../style/index";

const Home = ({ history }: any) => {
  const { root, paper, container } = HomeStyle();
  const [auth, setAuth] = React.useState({
    auth: "notAuth"
  });
  const [date, setDate] = React.useState(renderDateOneWeek());
  const [films, setFilms] = React.useState();

  React.useEffect(() => {
    const auth: any = window.sessionStorage.getItem("authentication");
    if (auth) {
      console.log("auth: ", auth);
      setAuth({ auth: auth });
    }
    const loadData = async () => {
      await loadAllFilms(setFilms);
    };
    loadData();
  }, []);

  const logoutHandle = () => {
    window.sessionStorage.removeItem("authentication");
    setAuth({ auth: "notAuth" });
  };

  return (
    <div className={root}>
      <Navbar authentication={auth.auth} logoutHandle={logoutHandle} />
      <Slider films={films} />
      <Grid container spacing={3} className={container}>
        <Grid item xs={2}>
          <LeftSide datetime={date} />
        </Grid>
        <Grid item xs={10}>
          <Paper className={paper}>
            <RightSide films={films} />
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

const loadAllFilms = async (
  action: React.Dispatch<React.SetStateAction<{}>>
) => {
  try {
    const response = await fetch("/film/findAll");
    const data = await response.json();
    console.log(data);
    action(data);
  } catch (ex) {
    console.log(ex.message);
  }
};

const renderDateOneWeek = () => {
  const curr = new Date();
  let week = [];
  let first = curr.getDate();
  const numberDaysOfMonth = new Date(
    curr.getFullYear(),
    curr.getMonth() + 1,
    0
  ).getDate();
  const currDate = curr.toISOString().slice(0, 10);
  const currDay = curr.toString().slice(0, 3);
  week.push({ date: currDate, day: currDay });
  for (let i = 0; i < 6; i++) {
    let month = curr.getMonth();
    first++;
    switch (numberDaysOfMonth) {
      case 31: {
        if (first > 31) {
          first = first % 31;
          month++;
        }
      }
      case 29: {
        if (first > 29) {
          first = first % 29;
          month++;
        }
      }
      case 28: {
        if (first > 28) {
          first = first % 28;
          month++;
        }
      }
      case 30: {
        if (first > 30) {
          first = first % 30;
          month++;
        }
      }
    }
    const newDate = new Date(curr.setMonth(month, first));
    const dateResult = newDate.toISOString().slice(0, 10);
    const dayResult = newDate.toString().slice(0, 3);
    week.push({ date: dateResult, day: dayResult });
  }
  return week;
};

export default Home;
