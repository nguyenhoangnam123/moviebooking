import * as React from "react";
import * as PropTypes from "prop-types";
import clsx from "clsx";
import { UserAccountStyle } from "Components/style/index";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Tooltip from "@material-ui/core/Tooltip";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import FilterListIcon from "@material-ui/icons/FilterList";
import Checkbox from "@material-ui/core/Checkbox";
import TablePagination from "@material-ui/core/TablePagination";
import {
  withStyles,
  Theme,
  createStyles,
  makeStyles,
  lighten
} from "@material-ui/core/styles";

interface EnhancedTableToolbarProps {
  numSelected: number;
  handleDelete: () => void;
}

interface EnhancedTableHeadProps {
  rowCount: number;
  numSelected: number;
  onSelectAllClick: (
    event: React.ChangeEvent<HTMLInputElement>,
    checked: boolean
  ) => void;
}

const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1)
    },
    highlight:
      theme.palette.type === "light"
        ? {
            color: theme.palette.secondary.main,
            backgroundColor: lighten(theme.palette.secondary.light, 0.85)
          }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark
          },
    spacer: {
      flex: "1 1 100%"
    },
    actions: {
      color: theme.palette.text.secondary
    },
    title: {
      flex: "0 0 auto"
    }
  })
);

const StyledTableCell = withStyles((theme: Theme) =>
  createStyles({
    head: {
      backgroundColor: "#343434",
      color: theme.palette.common.white
    },
    body: {
      fontSize: 14
    }
  })
)(TableCell);

const StyledTableRow = withStyles((theme: Theme) =>
  createStyles({
    root: {
      "&:nth-of-type(odd)": {
        backgroundColor: theme.palette.background.default
      }
    }
  })
)(TableRow);

const headRows: any = [
  {
    id: "id",
    numeric: false,
    disablePadding: true,
    label: "Ticket_code"
  },
  { id: "film", label: "Film" },
  { id: "duration", label: "Duration" },
  { id: "room", label: "Room" },
  { id: "seat_number", label: "Seat_number" },
  { id: "seat_type", label: "Seat_type" },
  { id: "date", label: "Date" },
  { id: "time", label: "Time" },
  { id: "price", label: "Price" }
];

const RightSide = () => {
  const { root, container, paper, table } = UserAccountStyle();
  const [ticket, setTicket] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(4);
  const [selected, setSelected] = React.useState<string[]>([]);

  React.useEffect(() => {
    const loadData = async () => {
      try {
        const response = await fetch("/ticket/findAll");
        const data = await response.json();
        setTicket(data);
      } catch (ex) {
        console.log(ex.message);
      }
    };
    loadData();
  }, []);

  const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
    const classes = useToolbarStyles();
    const { numSelected, handleDelete } = props;

    return (
      <Toolbar
        className={clsx(classes.root, {
          [classes.highlight]: numSelected > 0
        })}
      >
        <div className={classes.title}>
          {numSelected > 0 ? (
            <Typography color="inherit" variant="subtitle1">
              {numSelected} selected
            </Typography>
          ) : (
            <Typography variant="h6" id="tableTitle">
              Your Ticket List
            </Typography>
          )}
        </div>
        <div className={classes.spacer} />
        <div className={classes.actions}>
          {numSelected > 0 ? (
            <Tooltip title="Delete">
              <IconButton aria-label="Delete" onClick={handleDelete}>
                <DeleteIcon />
              </IconButton>
            </Tooltip>
          ) : (
            <Tooltip title="Filter list">
              <IconButton aria-label="Filter list">
                <FilterListIcon />
              </IconButton>
            </Tooltip>
          )}
        </div>
      </Toolbar>
    );
  };

  const EnhancedTableHead = (props: EnhancedTableHeadProps) => {
    const { onSelectAllClick, numSelected, rowCount } = props;
    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={numSelected === rowCount}
              onChange={onSelectAllClick}
              inputProps={{ "aria-label": "Select all desserts" }}
            />
          </TableCell>
          {headRows.map((item: any) => {
            const { id, label } = item;
            return <TableCell key={id}>{label}</TableCell>;
          })}
        </TableRow>
      </TableHead>
    );
  };

  const handleClick = (e: React.MouseEvent, id: string) => {
    const index = selected.indexOf(id);
    let newSelected: string[] = [];

    if (index === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (index === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (index === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (index > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, index),
        selected.slice(index + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleDelete = async () => {
    let newTickets = ticket;
    selected.map(item => {
      newTickets = newTickets.filter((ticket: any) => ticket._id != item);
    });
    setTicket(newTickets);
    try {
      await fetch("/ticket/delete", {
        method: "delete",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(selected)
      });
    } catch (ex) {
      console.log(ex.message);
    }
  };

  function handleSelectAllClick(event: React.ChangeEvent<HTMLInputElement>) {
    if (event.target.checked) {
      const newSelecteds = ticket.map((item: any) => item._id);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  }

  function handleChangeRowsPerPage(event: React.ChangeEvent<HTMLInputElement>) {
    console.log(event.target.value);
    setRowsPerPage(+event.target.value);
  }

  function handleChangePage(event: unknown, newPage: number) {
    setPage(newPage);
  }

  const isSelected = (name: any) => selected.indexOf(name) !== -1;

  EnhancedTableToolbar.propTypes = {
    numSelected: PropTypes.number.isRequired
  };

  return (
    <div className={root} style={{ display: "flex" }}>
      <Paper className={paper} style={{ flexGrow: 1 }}>
        <EnhancedTableToolbar
          numSelected={selected.length}
          handleDelete={handleDelete}
        />
        <Table className={table}>
          <EnhancedTableHead
            numSelected={selected.length}
            rowCount={ticket && ticket.length}
            onSelectAllClick={handleSelectAllClick}
          />
          <TableBody>
            {ticket &&
              ticket
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((ticket: any, index: any) => {
                  const isItemSelected = isSelected(ticket._id);
                  const labelId = `enhanced-table-checkbox-${index}`;
                  const { _id, seat, schedule, price, user } = ticket;
                  const { film, room, date, time } = schedule;
                  return (
                    <StyledTableRow
                      key={_id}
                      onClick={e => handleClick(e, _id)}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={isItemSelected}
                          inputProps={{ "aria-labelledby": labelId }}
                        />
                      </TableCell>
                      <StyledTableCell component="th" scope="row">
                        {_id}
                      </StyledTableCell>
                      <StyledTableCell component="th" scope="row">
                        {film.name}
                      </StyledTableCell>
                      <StyledTableCell>{film.duration}</StyledTableCell>
                      <StyledTableCell>{room}</StyledTableCell>
                      <StyledTableCell>{seat.seat_number}</StyledTableCell>
                      <StyledTableCell>{seat.seat_type}</StyledTableCell>
                      <StyledTableCell>{date}</StyledTableCell>
                      <StyledTableCell>{time}</StyledTableCell>
                      <StyledTableCell>{price} VND</StyledTableCell>
                    </StyledTableRow>
                  );
                })}
          </TableBody>
        </Table>
        <TablePagination
          rowsPerPageOptions={[1, 2, 3]}
          component="div"
          count={ticket && ticket.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            "aria-label": "Previous Page"
          }}
          nextIconButtonProps={{
            "aria-label": "Next Page"
          }}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
};
export default RightSide;
