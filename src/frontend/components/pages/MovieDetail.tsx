import * as React from "react";
import { Subscribe } from "unstated-x";
import TicketContainer from "Components/containers/TicketContainer";
import { Paper } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import { MovieDetailStyle } from "../style/index";
import MovieBookingModal from "../inc/MovieBookingModal";

const MovieDetail = () => {
  const { root, paper, container, imageFrame } = MovieDetailStyle();

  return (
    <div className={root}>
      <Subscribe to={[TicketContainer]}>
        {(ticketContainer: TicketContainer) => {
          const {
            id,
            name,
            content,
            origin,
            duration,
            media,
            type,
            releasedDate
          } = ticketContainer.state.chosenFilm;
          return (
            <Grid container className={container}>
              <Grid
                item
                xs={12}
                style={{
                  display: "flex",
                  justifyContent: "center",
                  justifyItems: "center"
                }}
              >
                <Paper className={paper}>
                  <Grid container>
                    <Grid item xs={6}>
                      <div
                        className={imageFrame}
                        style={{
                          background: `url(${media})`
                        }}
                      />
                    </Grid>
                    <Grid xs={6} style={{ textAlign: "left" }}>
                      <div
                        style={{
                          width: "100%",
                          padding: "15px"
                        }}
                      >
                        <h1>{name.toUpperCase()}</h1>
                        <p>
                          <b>Origin:</b> {origin}
                        </p>
                        <p>
                          <b>Duration:</b> {duration}
                        </p>
                        <p>
                          <b>Content:</b> {content}
                        </p>
                        <p>
                          <b>Type:</b>{" "}
                          {type.map((item, index) => {
                            return <span key={index}>{item + " "}</span>;
                          })}
                        </p>
                        <p>
                          <b>Released Date:</b> {releasedDate}
                        </p>
                        <Button
                          variant="contained"
                          color="secondary"
                          onClick={() => {
                            ticketContainer.openModal();
                            const loadData = async () => {
                              try {
                                const response = await fetch(
                                  `/schedule/findScheduleOfFilm/${id}`
                                );
                                const schedules = await response.json();
                                console.log(schedules);
                                ticketContainer.updateChange(
                                  "schedules",
                                  schedules
                                );
                              } catch (ex) {
                                console.log(ex.message);
                              }
                            };
                            loadData();
                          }}
                        >
                          Book Now
                        </Button>
                      </div>
                    </Grid>
                  </Grid>
                </Paper>
              </Grid>
              <MovieBookingModal ticketContainer={ticketContainer} />
            </Grid>
          );
        }}
      </Subscribe>
    </div>
  );
};

export default MovieDetail;
