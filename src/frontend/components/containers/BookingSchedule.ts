import { BookingScheduleProps } from "Interface/IBookingSchedule";
import { Container } from "unstated-x";

class BookingSchedule extends Container<BookingScheduleProps> {
  state: {
    date_time: [
      { date: "2019-06-15"; time: "08:00:00" },
      { date: "2019-06-15"; time: "11:00:00" },
      { date: "2019-06-16"; time: "08:00:00" },
      { date: "2019-06-16"; time: "11:00:00" }
    ];
    location: ["Ha Noi", "Ho Chi Minh"];
    schedules: [];
  };
}

export default BookingSchedule;
