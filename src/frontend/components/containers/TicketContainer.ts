import { Container } from "unstated-x";
import { BookingTicketProps, Ticket } from "Interface/index";
import { v4 as uuid } from "uuid";

const renderSeat = () => {
  const seat: any[] = [];
  ["A", "B", "C", "D", "E", "F", "G", "H", "K", "L"].map(a => {
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map(b => {
      const type = a == "B" ? "VIP" : "normal";
      seat.push({
        id: uuid(),
        seat_number: `${b + a}`,
        seat_type: type
      });
    });
  });
  return seat;
};

const renderDateOneWeek = () => {
  const curr = new Date();
  let week = [];
  let first = curr.getDate();
  const numberDaysOfMonth = new Date(
    curr.getFullYear(),
    curr.getMonth() + 1,
    0
  ).getDate();
  const currDate = curr.toISOString().slice(0, 10);
  const currDay = curr.toString().slice(0, 3);
  week.push({ date: currDate, day: currDay });
  for (let i = 0; i < 6; i++) {
    let month = curr.getMonth();
    first++;
    switch (numberDaysOfMonth) {
      case 31: {
        if (first > 31) {
          first = first % 31;
          month++;
        }
      }
      case 29: {
        if (first > 29) {
          first = first % 29;
          month++;
        }
      }
      case 28: {
        if (first > 28) {
          first = first % 28;
          month++;
        }
      }
      case 30: {
        if (first > 30) {
          first = first % 30;
          month++;
        }
      }
    }
    const newDate = new Date(curr.setMonth(month, first));
    const dateResult = newDate.toISOString().slice(0, 10);
    const dayResult = newDate.toString().slice(0, 3);
    week.push({ date: dateResult, day: dayResult });
  }
  return week;
};

class TicketContainer extends Container<BookingTicketProps> {
  state: BookingTicketProps = {
    location: [
      { name: "Ha Noi", room: ["FX Ba Trieu", "FX Thai Ha"] },
      { name: "Ho Chi Minh", room: ["FX Phu My Hung", "FX Cong Hoa"] }
    ],
    schedules: [
      {
        id: "12345",
        film: {
          id: "1234",
          name: "Lion King",
          content: "Lorem Ipsum is simply dummy text of the printing a...	",
          duration: "1h30m"
        },
        room: "FX Ba Trieu",
        emptySeat: 90,
        date: "2019-06-27",
        time: "08:00:00"
      },
      {
        id: "12346",
        film: {
          id: "1234",
          name: "Lion King",
          content: "Lorem Ipsum is simply dummy text of the printing a...	",
          duration: "1h30m"
        },
        room: "FX Ba Trieu",
        emptySeat: 90,
        date: "2019-06-27",
        time: "11:00:00"
      },
      {
        id: "12347",
        film: {
          id: "1234",
          name: "Lion King",
          content: "Lorem Ipsum is simply dummy text of the printing a...	",
          duration: "1h30m"
        },
        room: "FX Ba Trieu",
        emptySeat: 90,
        date: "2019-06-27",
        time: "15:00:00"
      },
      {
        id: "12348",
        film: {
          id: "1234",
          name: "Lion King",
          content: "Lorem Ipsum is simply dummy text of the printing a...	",
          duration: "1h30m"
        },
        room: "FX Thai Ha",
        emptySeat: 100,
        date: "2019-06-28",
        time: "09:00:00"
      },
      {
        id: "12349",
        film: {
          id: "1234",
          name: "Lion King",
          content: "Lorem Ipsum is simply dummy text of the printing a...	",
          duration: "1h30m"
        },
        room: "FX Phu My Hung",
        emptySeat: 100,
        date: "2019-06-28",
        time: "11:30:00"
      },
      {
        id: "12340",
        film: {
          id: "1234",
          name: "Lion King",
          content: "Lorem Ipsum is simply dummy text of the printing a...	",
          duration: "1h30m"
        },
        room: "FX Phu My Hung",
        emptySeat: 100,
        date: "2019-06-28",
        time: "15:00:00"
      }
    ],
    seat: renderSeat(),
    fullFillSeat: [
      { id: uuid(), seat_number: "1B", seat_type: "normal" },
      { id: uuid(), seat_number: "2B", seat_type: "normal" },
      { id: uuid(), seat_number: "3B", seat_type: "normal" },
      { id: uuid(), seat_number: "4B", seat_type: "normal" },
      { id: uuid(), seat_number: "5B", seat_type: "normal" },
      { id: uuid(), seat_number: "1C", seat_type: "VIP" },
      { id: uuid(), seat_number: "2C", seat_type: "VIP" },
      { id: uuid(), seat_number: "3C", seat_type: "VIP" },
      { id: uuid(), seat_number: "4C", seat_type: "VIP" },
      { id: uuid(), seat_number: "5C", seat_type: "VIP" }
    ],
    price: [
      {
        film: "Lion King",
        seat_type: "normal",
        value: 90
      },
      {
        film: "Lion King",
        seat_type: "VIP",
        value: 100
      }
    ],
    date: renderDateOneWeek(),
    chosenSeat: [],
    chosenSchedule: {
      id: "12345",
      film: {
        id: "1234",
        name: "Lion King",
        content: "Lorem Ipsum is simply dummy text of the printing a...	",
        duration: "1h30m"
      },
      room: "FX Ba Trieu",
      emptySeat: 90,
      date: "2019-06-15",
      time: "08:00:00"
    },
    chosenFilm: {
      id: "1234",
      name: "Lion King",
      content: "Lorem Ipsum is simply dummy text of the printing a...	",
      origin: "Disney, USA",
      type: ["cartoon", "fiction"],
      duration: "1h30m",
      media:
        "https://static5.businessinsider.com/image/5c739bc3dde86718564ddaaa-1688/the-lion-king-poster.jpg",
      releasedDate: "2019-06-23"
    },
    chosenLocation: { name: "Ha Noi", room: ["FX Ba Trieu", "FX Thai Ha"] },
    chosenDate: {
      date: "2019-06-27",
      day: "Thu"
    },
    tickets: [],
    message: "",
    open: false
  };

  addChosenSeat = (seat_number: string) => {
    const { fullFillSeat, chosenSeat, tickets } = this.state;
    const userId = sessionStorage.getItem("_id");
    const userEmail = sessionStorage.getItem("email");
    const newFullFillSeat = fullFillSeat;
    const newChosenSeat = chosenSeat;
    const newTickets = tickets;

    let newTicket = {
      id: uuid(),
      schedule: this.state.chosenSchedule,
      seat: {
        id: "",
        seat_number: "",
        seat_type: ""
      },
      price: {
        seat_type: "",
        film: "",
        value: 0
      },
      user: {
        _id: userId,
        email: userEmail
      }
    };

    this.state.seat.map(seat => {
      if (seat.seat_number == seat_number) {
        newFullFillSeat.push(seat);
        newChosenSeat.push(seat);
        newTicket.seat = seat;
        newTicket.price = this.searchPrice(seat.seat_type)!;
        newTickets.push(newTicket);

        this.setState({
          fullFillSeat: newFullFillSeat,
          chosenSeat: newChosenSeat,
          tickets: newTickets
        });
      }
    });
  };

  deleteTicket = (ticket: Ticket) => {
    const { tickets, fullFillSeat, chosenSeat } = this.state;
    const { id, seat } = ticket;
    const seat_number = seat.seat_number;
    const newTickets = tickets.filter(ticket => ticket.id != id);
    const newFullFillSeat = fullFillSeat.filter(
      seat => seat.seat_number != seat_number
    );
    const newChosenSeat = chosenSeat.filter(
      seat => seat.seat_number != seat_number
    );
    this.setState({
      tickets: newTickets,
      fullFillSeat: newFullFillSeat,
      chosenSeat: newChosenSeat
    });
  };

  deleteAllTickets = () => {
    const { tickets, fullFillSeat, chosenSeat } = this.state;
    let newFullFillSeat = fullFillSeat;
    let newChosenSeat = chosenSeat;
    tickets.map(ticket => {
      const { seat_number } = ticket.seat;
      newFullFillSeat = newFullFillSeat.filter(
        seat => seat.seat_number != seat_number
      );
      newChosenSeat = newChosenSeat.filter(
        seat => seat.seat_number != seat_number
      );
    });
    tickets.length = 0;
    this.setState({
      fullFillSeat: newFullFillSeat,
      chosenSeat: newChosenSeat
    });
  };

  updateChange = async (state: string, value: any) => {
    await this.setState((prevState: BookingTicketProps) => ({
      ...prevState,
      [state]: value
    }));
  };

  searchPrice = (seat_type: string) => {
    let result;
    this.state.price.map(price => {
      if (price.seat_type == seat_type) {
        result = price;
      }
    });
    return result;
  };

  openModal = () => {
    this.setState({ open: true });
  };

  closeModal = () => {
    this.setState({ open: false });
  };
}

export default TicketContainer;
