const film = [
  {
    id: "1234",
    name: "Lion King",
    content: "Lorem Ipsum is simply dummy text of the printing a...	",
    origin: "Disney, USA",
    type: ["cartoon", "fiction"],
    duration: "1h30m",
    media:
      "https://static5.businessinsider.com/image/5c739bc3dde86718564ddaaa-1688/the-lion-king-poster.jpg",
    releasedDate: "2019-06-23"
  },
  {
    id: "1235",
    name: "Aladin",
    content: "Lorem Ipsum is simply dummy text of the printing a...	",
    origin: "Disney, USA",
    type: ["fiction"],
    duration: "2h30m",
    media:
      "https://lumiere-a.akamaihd.net/v1/images/b_aladdin_header_17903_4ec6b753.jpeg?region=0,0,2048,640",
    releasedDate: "2019-06-24"
  },
  {
    id: "1236",
    name: "Endgame",
    content: "Lorem Ipsum is simply dummy text of the printing a...	",
    origin: "Disney, USA",
    type: ["fiction", "superhero"],
    duration: "2h30m",
    media:
      "https://lumiere-a.akamaihd.net/v1/images/b_avengersendgame_horizontal_nowplaying_17793_5531b541.jpeg?region=0,0,2048,599",
    releasedDate: "2019-06-25"
  }
];
export default film;
