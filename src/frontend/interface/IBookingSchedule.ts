import { Schedule } from "./ITicketContainer";

export interface BookingScheduleProps {
  date_time: DateTime[];
  schedules: Schedule[];
}

export interface DateTime {
  date: string;
  time: string;
}
