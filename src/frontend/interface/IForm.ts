import { FormikProps } from "formik";

export type FormUserDetailsProps = {
  firstname: string;
  lastname: string;
  gender: string;
  age: string;
};
export interface FormProps {
  firstname: string;
  lastname: string;
  gender: string;
  age: string;
  email: string;
  password: string;
}

export type FormEmailAndPasswordProps = {
  email: string;
  password: string;
};

export interface FormRegisterProps extends FormikProps<FormProps> {
  nextStep: () => void;
}

export interface FormRegistrationProps
  extends FormikProps<FormEmailAndPasswordProps> {
  nextStep: () => void;
  onChangeState: (name: string, value: any) => void;
}

export interface FormUserDetailProps extends FormikProps<FormUserDetailsProps> {
  nextStep: () => void;
  prevStep: () => void;
  onChangeState: (name: string, value: any) => void;
  submitFormSignUp: () => void;
}
