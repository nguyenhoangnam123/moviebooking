import { string } from "yup";

export interface TicketProps {
  _id: string;
  seat: SeatProps;
  schedule: ScheduleProps;
  price: number;
  user: UserProps;
}

export interface UserProps {
  _id: string;
  firstname: string;
  lastname: string;
  email: string;
}

export interface ScheduleProps {
  _id: string;
  film: FilmProps;
  room: RoomProps;
  date: Date;
  time: TimeRanges;
}

export interface FilmProps {
  _id: string;
  name: string;
  duration: string;
}

export interface RoomProps {
  _id: string;
  name: string;
  location: string;
}

export interface LocationProps {
  _id: string;
  name: string;
}

export interface SeatProps {
  _id: string;
  seat_number: string;
  seat_type: string;
}
