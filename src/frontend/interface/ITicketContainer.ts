export interface TicketContainerProps {
  state: BookingTicketProps;
  addChosenSeat: (seat: string) => void;
  deleteTicket: (ticket: Ticket) => void;
  deleteAllTickets: () => void;
  updateChange: (state: string, value: any) => void;
  openModal: () => void;
  closeModal: () => void;
}

export interface BookingTicketProps {
  chosenFilm: Film;
  location: Location[];
  schedules: Schedule[];
  seat: Seat[];
  fullFillSeat: Seat[];
  chosenSchedule: Schedule;
  chosenSeat: Seat[];
  price: Price[];
  message: string;
  tickets: Ticket[];
  date: Date[];
  chosenLocation: Location;
  chosenDate: Date;
  open: boolean;
}

export interface Ticket {
  id: string;
  schedule: Schedule;
  seat: Seat;
  price: Price;
}

export interface Schedule {
  id: string;
  film: {
    id: string;
    name: string;
    content: string;
    duration: string;
  };
  room: string;
  emptySeat: number;
  date: string;
  time: string;
}

export interface Seat {
  id: string;
  seat_number: string;
  seat_type: string;
}

export interface Film {
  id: string;
  name: string;
  content: string;
  type: string[];
  origin: string;
  duration: string;
  media: string;
  releasedDate: string;
}

export interface Price {
  film: string;
  seat_type: string;
  value: number;
}

export interface Date {
  date: string;
  day: string;
}

export interface Location {
  name: string;
  room: string[];
}
