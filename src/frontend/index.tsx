import * as React from "react";
import * as ReactDOM from "react-dom";
import App from "./components/App";
import "./css/main.css";

const root = document.createElement("div");
root.setAttribute("id", "root");
document.body.appendChild(root);

ReactDOM.render(<App />, root);
